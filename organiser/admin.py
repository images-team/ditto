from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from organiser.models import (
    Profile,
    Test,
    DILanguage,
    Architecture,
    Errata,
    MachineProfile,
)

# This package is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation
# .
# This package is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
# .
# You should have received a copy of the GNU Affero General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

# pylint: disable=function-redefined


class ProfileInline(admin.StackedInline):
    """
    Exposes the default owner override class
    in the Django admin interface
    """

    model = Profile


class UserAdmin(UserAdmin):  # pylint: disable=function-redefined
    """
    Defines the override class for DefaultOwnerInline
    """

    inlines = (ProfileInline,)


class MachineProfileAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "architecture",
        "hardware",
        "boot_method",
        "comment",
        "owner",
        "sound",
        "screen_keybd",
        "serial",
        "ethernet",
        "wifi",
        "network",
    )


class TestAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "claimer",
        "image",
        "point",
        "number",
        "profile",
        "updated",
        "errata",
        "result",
    )


#  Setup the override in the django admin interface at startup.
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Architecture)
admin.site.register(DILanguage)
admin.site.register(Errata)
admin.site.register(Test, TestAdmin)
admin.site.register(MachineProfile, MachineProfileAdmin)
