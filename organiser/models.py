from nose.tools import nottest
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver

# This package is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation
# .
# This package is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
# .
# You should have received a copy of the GNU Affero General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


# Enable debug messages being printed during unit tests
DEBUG = False


def vprint(msg):
    if DEBUG:
        print(msg)


class Profile(models.Model):
    """
    Extend the authenticated User to provide details
    like IRC nick.
    https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone

    State: Active | Awaiting moderation | Emeritus | Blocked
    create account template & view, linked from front page.
    email all the admins with new account in Awaiting Moderation status.

    If in blocked - Django won't permit a password reset - Active: false in Django.
    emeritus: requires a password reset but allows it.
    To set emeritus, just change their password to a strong one without telling them.
    ./manage.py helper to do this automatically if no login in N timeframe.
    awaiting_moderation:
        working password
        allowed to login
        allowed to create & edit machine profiles
        NOT allowed to claims tests
    superuser - django admin interface
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nick = models.CharField(max_length=128, blank=True)

    STATUS_WAIT, STATUS_ACTIVE, STATUS_EMERITUS, STATUS_BLOCKED = range(4)

    STATUS_CHOICES = (
        (STATUS_WAIT, _("Moderated")),
        (STATUS_ACTIVE, _("Active")),
        (STATUS_EMERITUS, _("Emeritus")),
        (STATUS_BLOCKED, _("Blocked")),
    )

    status = models.IntegerField(choices=STATUS_CHOICES, default=STATUS_WAIT)

    def clean(self):
        if self.user.is_active and self.status in [
            self.STATUS_EMERITUS,
            self.STATUS_BLOCKED,
        ]:
            self.user.is_active = False
            self.user.save()
            self.save()

        if not self.user.is_active and self.status in [
            self.STATUS_WAIT,
            self.STATUS_ACTIVE,
        ]:
            self.user.is_active = True
            self.user.save()
            self.save()

    # known test setups.
    # VM / UEFI only or bare metal arm64 etc.
    # at least 1 created at or before claiming the first test.
    # filter tests & prepopulate based on known profiles or add a new profile.

    def __str__(self):
        return self.user.username


class Architecture(models.Model):

    name = models.CharField(
        primary_key=True,
        verbose_name=_("Architecture"),
        help_text=_("Debian architecture name"),
        max_length=100,
        editable=True,
    )

    def __str__(self):
        return self.name


class DILanguage(models.Model):

    name = models.CharField(
        primary_key=True,
        verbose_name=_("DI language"),
        help_text=_("Language chosen for Debian Installer"),
        max_length=50,
        editable=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("DI Language")
        verbose_name_plural = _("DI Languages")


class MachineProfile(models.Model):
    """
    Validator & data entry on Profile page
    User must have at least one profile
    Profiles are used to filter the offered tests
    Only offered tests which match existing one of user profiles
    """

    architecture = models.ForeignKey(
        Architecture, related_name="profiles", editable=True
    )

    HARDWARE_BARE_METAL, HARDWARE_VIRTUAL = range(2)

    HARDWARE_CHOICES = (
        (HARDWARE_BARE_METAL, _("Bare metal machine")),
        (HARDWARE_VIRTUAL, _("Virtualised emulation")),
    )

    hardware = models.IntegerField(choices=HARDWARE_CHOICES, editable=True)

    # "VM" or "bare metal" with text field description.
    # VM used or describe your physical hardware
    # VM: QEMU | QEMU/KVM | VirtualBox | VMWare | Xen
    # Bare: Thinkpad T410 | Dell Optiplex 330
    # We don't particularly care about your CPU specifically
    # a lot more data in Errata.

    BOOT_BIOS, BOOT_UEFI = range(2)

    BOOT_CHOICES = ((BOOT_BIOS, "BIOS"), (BOOT_UEFI, "UEFI"))

    boot_method = models.IntegerField(choices=BOOT_CHOICES, editable=True)

    name = models.CharField(
        # only unique for this user
        # how all users select the profile
        # must not be null or blank
        verbose_name=_("Name"),
        help_text=_("User friendly name for this machine profile"),
        max_length=100,
        editable=True,
    )

    sound = models.BooleanField(
        default=False,
        verbose_name=_("Sound"),
        help_text=_("Whether this machine profile has working sound functionality"),
    )

    command_line = models.CharField(
        # Only set for Virtual
        verbose_name=_("Command line"),
        help_text=_(
            "Command line used to launch the VM or the name of your chosen GUI"
        ),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    emulator = models.CharField(
        # only for Virtual
        verbose_name=_("Emulator"),
        help_text=_("Name of the emulator used in a virtual test"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    manufacturer = models.CharField(
        # only for bare metal
        verbose_name=_("Manufacturer"),
        help_text=_("Manufacturer of the bare metal hardware"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    model_name = models.CharField(
        verbose_name=_("Model"),
        help_text=_("Model name, if any, of the bare metal"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    cpu_type = models.CharField(
        verbose_name=_("CPU"),
        help_text=_("CPU description (optional, can be used by bare metal or virtual)"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    ram_size = models.CharField(
        verbose_name=_("RAM"),
        help_text=_("RAM size (include units)"),
        max_length=100,
        editable=True,
    )

    disc_size = models.CharField(
        verbose_name=_("Disc size"),
        help_text=_("Disc size for install (include units)"),
        max_length=100,
        editable=True,
    )

    resolution = models.CharField(
        verbose_name=_("Resolution"),
        help_text=_("Display resolution (optional)"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    comment = models.CharField(
        verbose_name=_("Comment"),
        help_text=_("Other comments"),
        max_length=100,
        editable=True,
        null=True,
        blank=True,
        default=None,
    )

    screen_keybd = models.BooleanField(
        default=False,
        verbose_name=_("Screen and Keyboard"),
        help_text=_("Whether both a display and keyboard are available."),
    )

    serial = models.BooleanField(
        default=False,
        verbose_name=_("Serial"),
        help_text=_("Whether DI can use a serial console."),
    )

    ethernet = models.BooleanField(
        default=False,
        verbose_name=_("Ethernet"),
        help_text=_("Whether DI can use ethernet networking"),
    )

    wifi = models.BooleanField(
        default=False,
        verbose_name=_("WiFi"),
        help_text=_("Whether DI can use WiFi networking"),
    )

    NETWORK_IPV4_ONLY = 0
    NETWORK_IPV6_ONLY = 1
    NETWORK_IPV4_FALLBACK = 2
    NETWORK_IPV6_FALLBACK = 3
    NETWORK_NONE = 4

    NETWORK_CHOICES = (
        (NETWORK_IPV4_ONLY, _("Network only has IPV4 support.")),
        (NETWORK_IPV6_ONLY, _("Network only has IPV6 support.")),
        (NETWORK_IPV4_FALLBACK, _("Network is IPv6 with IPV4 fallback.")),
        (NETWORK_IPV6_FALLBACK, _("Network is IPv4 with IPv6 fallback.")),
        (NETWORK_NONE, _("No network is available of any kind")),
    )

    network = models.IntegerField(
        choices=NETWORK_CHOICES,
        default=NETWORK_NONE,
        verbose_name=_("Network"),
        help_text=_("Type of available network connection or none."),
    )

    owner = models.ForeignKey(User, related_name="profiles", default=None)

    def clean(self):
        # command_line & emulator only for Virtual
        # manufacturer & model_name only for bare metal
        if self.hardware == self.HARDWARE_BARE_METAL:
            if self.command_line or self.emulator:
                raise ValidationError(
                    _(
                        "Bare metal profiles must not set command_line or emulator fields."
                    )
                )
            if not self.manufacturer:
                raise ValidationError(_("Bare metal profiles must set Manufacturer."))
            if not self.model_name:
                raise ValidationError(_("Bare metal profiles must set Model Name."))
        if self.hardware == self.HARDWARE_VIRTUAL:
            if self.manufacturer:
                raise ValidationError(
                    _("Virtual profiles must not set manufacturer - use emulator.")
                )
            if not self.command_line:
                raise ValidationError(
                    _("Virtual profiles need a command line or GUI name.")
                )
            if self.model_name:
                raise ValidationError(_("Virtual profiles must not set model_name."))

    def __str__(self):
        return self.name


# Filesystem: import list of choices by admin|VCS
# Want to test: each different type of FS on each image as root mount point.
#    exceptions: FS without permissions (VFAT | FAT) - test as data partition.
# disk setup: simple | LVM | RAID | encrypted | LVM encrypted | LVM RAID | RAID LVM
# partitions: swap & \\ | swap & multiple
#    note about EFI partition requirement


# pylint: disable=unused-argument
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Errata(models.Model):

    content = models.TextField(verbose_name="Content", help_text="Errata content")

    profile = models.ForeignKey(MachineProfile, related_name="errata")

    class Meta:
        verbose_name_plural = _("Errata")

    def __str__(self):
        return "Test {0} : {1}".format(
            ", ".join(
                [
                    "ID {0} number {1} by {2}".format(
                        test.pk, test.number, test.claimer
                    )
                    for test in self.test_records.all()
                ]
            ),
            self.content[:64],
        )


@nottest
class Test(models.Model):
    """
    Most User objects would have permission to add and edit
    Test objects but nothing else.
    """

    number = models.CharField(
        # to be loaded from YAML configuration.
        max_length=128,
        verbose_name=_("Number"),
        help_text=_("Test number with optional suffix"),
        editable=True,
        null=True,
    )

    claimer = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        editable=True,
        blank=True,
        verbose_name=_("Claimer"),
        help_text=_("User claiming this test"),
        null=True,
    )

    # timestamp - claim & completed.
    # show users all tests that this user has currently claimed.
    created = models.DateTimeField(auto_now_add=True)

    updated = models.DateTimeField(auto_now=True)

    # FILENAME not description
    image = models.CharField(
        # to be loaded from YAML configuration.
        max_length=2048,
        verbose_name=_("Image"),
        help_text=_("Filename of the tested image"),
        editable=True,
    )

    INSTALL_GRAPHICAL = 0
    INSTALL_TEXT = 1
    INSTALL_SPEECH = 2
    INSTALL_GRAPHICAL_EXPERT = 3
    INSTALL_TEXT_EXPERT = 4
    INSTALL_UNKNOWN = 5

    INSTALL_CHOICES = (
        (INSTALL_GRAPHICAL, _("Graphical install")),
        (INSTALL_TEXT, _("Text install")),
        (INSTALL_GRAPHICAL_EXPERT, _("Graphical Expert install")),
        (INSTALL_TEXT_EXPERT, _("Text Expert install")),
        (INSTALL_SPEECH, _("Speech synthesis install")),
        (INSTALL_UNKNOWN, _("Unknown")),
    )

    mode = models.IntegerField(
        # to be loaded from YAML configuration.
        choices=INSTALL_CHOICES,
        default=INSTALL_UNKNOWN,
        editable=True,
    )

    disk = models.CharField(
        # to be loaded from YAML configuration.
        max_length=1024,
        verbose_name=_("Disk"),
        help_text=_("Disk configuration"),
        editable=True,
        blank=True,
        null=True,
    )

    filesystem = models.CharField(
        # to be loaded from YAML configuration.
        max_length=1024,
        verbose_name=_("FS"),
        help_text=_("Filesystem selection or selections"),
        editable=True,
        blank=True,
        null=True,
    )

    desktop = models.CharField(
        # to be loaded from YAML configuration.
        max_length=1024,
        verbose_name=_("Desktop"),
        help_text=_("Desktop selection or selections"),
        editable=True,
        blank=True,
        null=True,
    )

    point = models.CharField(
        # set from the YAML configuration in place at Test creation
        max_length=1024,
        verbose_name=_("Point release"),
        help_text=_("Debian Point Release"),
        editable=True,
    )

    NETWORK_IPV4_ONLY = 0
    NETWORK_IPV6_ONLY = 1
    NETWORK_IPV4_FALLBACK = 2
    NETWORK_IPV6_FALLBACK = 3
    NETWORK_NONE = 4

    NETWORK_CHOICES = (
        (NETWORK_IPV4_ONLY, _("Network only has IPV4 support.")),
        (NETWORK_IPV6_ONLY, _("Network only has IPV6 support.")),
        (NETWORK_IPV4_FALLBACK, _("Network is IPv6 with IPV4 fallback.")),
        (NETWORK_IPV6_FALLBACK, _("Network is IPv4 with IPv6 fallback.")),
        (NETWORK_NONE, _("No network is available of any kind")),
    )

    network = models.IntegerField(
        choices=NETWORK_CHOICES,
        default=NETWORK_NONE,
        verbose_name=_("Network"),
        help_text=_("Type of available network connection or none."),
    )

    language = models.ForeignKey(DILanguage, related_name="tests", editable=True)

    profile = models.ForeignKey(MachineProfile, related_name="test_records")

    errata = models.ForeignKey(
        Errata, related_name="test_records", blank=True, null=True
    )

    RESULT_UNCLAIMED = 0
    RESULT_CLAIMED = 1
    RESULT_PASS = 2
    RESULT_FAIL = 3
    RESULT_ABANDONED = 4

    RESULT_CHOICES = (
        (RESULT_UNCLAIMED, _("unclaimed")),
        (RESULT_CLAIMED, _("claimed")),
        (RESULT_PASS, _("pass")),
        (RESULT_FAIL, _("fail")),  # require Errata
        (RESULT_ABANDONED, _("abandoned")),  # > 2hrs
    )

    result = models.IntegerField(
        choices=RESULT_CHOICES, default=RESULT_UNCLAIMED, editable=True
    )

    # if not updated for 1 hour, make available to other matching profiles.
    # allow abandoned to be set, on timeout, or manually & taken out of abandoned manually.

    def clean(self):
        unclaimed = timezone.now() - timezone.timedelta(hours=1)
        vprint(unclaimed)
        aged = timezone.now() - timezone.timedelta(hours=2)
        vprint(aged)
        if self.updated:
            vprint(self.updated)
            if self.updated < unclaimed and self.result == self.RESULT_CLAIMED:
                vprint("Claim not updated for 1 hour, setting unclaimed")
                self.result = self.RESULT_UNCLAIMED
            elif self.updated < aged:
                vprint("Claim is aged, setting abandoned")
                # this isn't really a timeout
                self.result = self.RESULT_ABANDONED
            elif self.claimer and self.result == self.RESULT_UNCLAIMED:
                vprint("Newly created claim setting as claimed")
                # newly created tests start as claimed.
                self.result = self.RESULT_CLAIMED
            self.save()

    def __str__(self):
        return "{0} {1} {2} {3}".format(
            self.claimer, self.RESULT_CHOICES[self.result], self.profile, self.image
        )
