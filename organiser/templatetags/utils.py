from django.shortcuts import get_object_or_404
from django import template
from django.contrib.auth.models import User
from organiser.models import Profile, Test, MachineProfile

register = template.Library()


@register.filter
def user_is_active(user):
    """ Returns whather the Ditto Profile is active """
    prof = Profile.objects.get(user=user)
    if prof:
        if prof.status == Profile.STATUS_ACTIVE:
            return True
    return False


@register.filter
def user_status(user):
    """ Returns the Ditto Profile for the request.user """
    prof = Profile.objects.get(user=user)
    if prof:
        return Profile.STATUS_CHOICES[prof.status][1]
    return "Unknown user status"


@register.filter
def user_name(user):
    """ Turns the username into full name, first and last name """
    if user.first_name and user.last_name:
        return "{0} {1}".format(user.first_name, user.last_name)
    return user.username


@register.filter
def print_result(test_item):
    return Test.RESULT_CHOICES[test_item][1]


@register.filter
def print_install_mode(test_mode):
    return Test.INSTALL_CHOICES[test_mode][1]


@register.filter
def print_network(test_network):
    return Test.NETWORK_CHOICES[test_network][1]


@register.filter
def print_hardware(hardware):
    return MachineProfile.HARDWARE_CHOICES[hardware][1]


@register.filter
def print_boot(method):
    return MachineProfile.BOOT_CHOICES[method][1]
