from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.db.models import Q
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.forms import UserCreationForm
from organiser.forms import (
    TestForm,
    VirtualProfileForm,
    BareMetalProfileForm,
    UpdateForm,
    AddErrata,
)
from organiser.utils import valid_user_required, ReleaseSet
from organiser.models import MachineProfile, Test, Profile, Errata


# This package is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation
# .
# This package is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
# .
# You should have received a copy of the GNU Affero General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


def index(request):
    v_prof = []
    b_prof = []
    myclaims = None
    completed = None
    release_set = ReleaseSet()
    claims = Test.objects.filter(
        result__in=[Test.RESULT_CLAIMED, Test.RESULT_ABANDONED, Test.RESULT_UNCLAIMED],
        point__in=release_set.point_releases,
    ).order_by("point", "number")
    if not isinstance(request.user, AnonymousUser):
        v_prof = MachineProfile.objects.filter(
            owner=request.user, hardware=MachineProfile.HARDWARE_VIRTUAL
        )
        b_prof = MachineProfile.objects.filter(
            owner=request.user, hardware=MachineProfile.HARDWARE_BARE_METAL
        )
        prof = Profile.objects.get(user=request.user)
        myclaims = Test.objects.filter(
            claimer=prof,
            result__in=[
                Test.RESULT_CLAIMED,
                Test.RESULT_UNCLAIMED,
                Test.RESULT_ABANDONED,
            ],
        ).order_by("number")
        claims = Test.objects.filter(
            Q(result__in=[Test.RESULT_CLAIMED, Test.RESULT_UNCLAIMED])
            & ~Q(claimer=prof)
        ).order_by("point", "number")
    completed = Test.objects.filter(
        result__in=[Test.RESULT_PASS, Test.RESULT_FAIL]
    ).order_by("-updated", "point", "number")
    return render(
        request,
        "organiser/index.html",
        {
            "vm_profiles": v_prof,
            "bare_profiles": b_prof,
            "myclaims": myclaims,
            "claims": claims,
            "completed": completed,
            "version": release_set.filename,
        },
    )


@valid_user_required
def offer_test(request, pk):
    profile_list = MachineProfile.objects.filter(pk=pk)
    if not profile_list:
        raise Http404
    profile = profile_list[0]
    release_set = ReleaseSet()
    # match this profile against priorities.
    c = 0
    # removes from options any claims already processed, by number only.
    options = release_set.find_image(profile)
    image_numbers = []
    for image in options:
        image_numbers.append((c, image.number))
        c += 1

    # If this is a POST request then process the Form data
    if request.method == "POST":
        # Create a form instance and populate it with data from the request (binding):
        form = TestForm(request.POST, initial={"numbers": image_numbers})
        if form.is_valid():
            number_str = form.cleaned_data.get("number")
            if not number_str.isdigit():
                raise Http404
            number = int(number_str)
            number = "%s" % image_numbers[number][1]
            network = form.cleaned_data.get("network")
            claimed = form.save()
            image = release_set.lookup_number(number)
            claimed.image = image.name
            image.populate_test(claimed)
            # need the image filename
            claimed.claimer = Profile.objects.get(user=request.user)
            claimed.network = Test.NETWORK_CHOICES[network][0]
            claimed.number = number
            claimed.language = form.cleaned_data.get("language")
            claimed.result = form.cleaned_data.get("result")
            claimed.save()
            # needs to be changed later to the update form view.
            return redirect("index")
    else:
        # Let user decide ipv6 vs ipv4
        values = {
            "claimer": request.user,
            "network": profile.ethernet,
            "profile": profile,
            "numbers": image_numbers,
            "number": "empty",
        }
        form = TestForm(initial=values)

    context = {"form": form, "profile": profile, "options": options}

    return render(request, "organiser/claim.html", context)


def testlog(request, pk):
    item = get_object_or_404(Test, pk=pk)
    context = {"test": item}
    return render(request, "organiser/testlog.html", context)


@valid_user_required
def update_test(request, pk):
    claimed = get_object_or_404(Test, pk=pk)
    if request.method == "POST":
        form = UpdateForm(request.POST, instance=claimed)
        if form.is_valid():
            updated = form.save()
            claimed.result = updated.result
            claimed.save(update_fields=["result"])
            if updated.result == Test.RESULT_ABANDONED:
                claimed.claimer = None
                claimed.save(update_fields=["claimer"])
                return redirect("index")
            if updated.result == Test.RESULT_FAIL:
                return redirect("/errata/{0}".format(claimed.pk))
            return redirect("index")
    else:
        values = {
            "claimer": request.user,
            "number": claimed.number,
            "language": claimed.language,
            "profile": claimed.profile,
            "image": claimed.image,
            "point": claimed.point,
            "boot": claimed.profile.boot_method,
            "mode": claimed.mode,
            "disk": claimed.disk,
            "network": claimed.network,
            "filesystem": claimed.filesystem,
            "desktop": claimed.desktop,
        }
        form = UpdateForm(initial=values, instance=claimed)

    context = {"form": form, "test": claimed}

    return render(request, "organiser/update.html", context)


def test_errata(request, pk):
    claimed = get_object_or_404(Test, pk=pk)
    if request.method == "POST":
        form = AddErrata(request.POST)
        if form.is_valid():
            errata = form.save()
            claimed.errata = errata
            if claimed.result == Test.RESULT_ABANDONED:  # this needs a unit test!
                claimed.claimer = None
            claimed.save(update_fields=["errata", "claimer"])
            return redirect("index")
    else:
        values = {"test": pk, "profile": claimed.profile}
        form = AddErrata(initial=values)
    context = {"form": form, "test": claimed}

    return render(request, "organiser/errata.html", context)


def view_errata(request, pk):
    errata = get_object_or_404(Errata, pk=pk)
    context = {"errata": errata}
    return render(request, "organiser/viewerrata.html", context)


def create_account(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect("index")
    else:
        form = UserCreationForm()
    return render(request, "organiser/createuser.html", {"form": form})


@valid_user_required
def create_virtual_profile(request):
    if request.method == "POST":
        form = VirtualProfileForm(request.POST)
        if form.is_valid():
            v_obj = form.save()
            v_obj.owner = request.user
            v_obj.save(update_fields=["owner"])
            return redirect("index")
    else:
        values = {"hardware": MachineProfile.HARDWARE_VIRTUAL, "owner": request.user}
        form = VirtualProfileForm(initial=values)
    return render(request, "organiser/createvirtual.html", {"form": form})


@valid_user_required
def create_bare_profile(request):
    if request.method == "POST":
        form = BareMetalProfileForm(request.POST)
        form.hardware = MachineProfile.HARDWARE_BARE_METAL
        if form.is_valid():
            b_obj = form.save()
            b_obj.owner = request.user
            b_obj.save(update_fields=["owner"])
            return redirect("index")
    else:
        values = {"hardware": MachineProfile.HARDWARE_BARE_METAL, "owner": request.user}
        form = BareMetalProfileForm(initial=values)
    return render(request, "organiser/createbare.html", {"form": form})


@valid_user_required
def amend_profile(request, pk):
    m_prof = get_object_or_404(MachineProfile, pk=pk)
    values = {
        "name": m_prof.name,
        "architecture": m_prof.architecture,
        "manufacturer": m_prof.manufacturer,
        "hardware": m_prof.hardware,
        "owner": request.user,
        "model_name": m_prof.model_name,
        "cpu_type": m_prof.cpu_type,
        "ram_size": m_prof.ram_size,
        "disc_size": m_prof.disc_size,
        "boot_method": m_prof.boot_method,
        "sound": m_prof.sound,
        "resolution": m_prof.resolution,
        "screen_keybd": m_prof.screen_keybd,
        "serial": m_prof.serial,
        "ethernet": m_prof.ethernet,
        "wifi": m_prof.wifi,
        "network": m_prof.network,
    }
    if m_prof.hardware == MachineProfile.HARDWARE_BARE_METAL:
        if request.method == "POST":
            form = BareMetalProfileForm(request.POST, instance=m_prof)
            if form.is_valid():
                form.save()
                return redirect("index")
        else:
            form = BareMetalProfileForm(initial=values, instance=m_prof)
        return render(request, "organiser/updatebare.html", {"form": form})
    if m_prof.hardware == MachineProfile.HARDWARE_VIRTUAL:
        if request.method == "POST":
            form = VirtualProfileForm(request.POST, instance=m_prof)
            if form.is_valid():
                form.save()
                return redirect("index")
        else:
            form = VirtualProfileForm(initial=values, instance=m_prof)
        return render(request, "organiser/updatevirtual.html", {"form": form})
    else:
        raise Http404
